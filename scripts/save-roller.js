import {critCheck} from "./crit-test.js";
import {showResults} from "./show-results.js";

export async function forceDCSave(action, dc, id) {



   let saveName = 'error';

    if (action.includes('reflex')) {
        saveName = 'reflex';
    }
    if (action.includes('will')) {
        saveName = 'will';
    }
    if (action.includes('fortitude')) {
        saveName = 'fortitude';
    }
    if (saveName === 'error') {
        ui.notifications.error("Invalid save type. Make sure this particular spell has a save type in its config, not all start with one.")
        return;
    }


    let tokens = canvas.tokens.controlled[0];

    if (!tokens){
        ui.notifications.error("You have not selected your token. Please select the caster")
        return;
    }


    if (!tokens.actor.data.items.find(item => item._id === id)){
    ui.notifications.error("The spell was not found on the selected token. Please select the caster of the spell.")
        return;
    }
    
    let spellTraits = tokens.actor.data.items.find(item => item._id === id).data.traits.value



    dc = parseInt(dc[0]);
    

    const messages = await Promise.all([...game.user.targets].map(async t => {

        t.actor.data.data.saves[action].roll(event, spellTraits, (result) => {
            let message;
        //Success step meaning:
        // 3 = Critical
        // 2 = success
        // 1 = failure
        // 0 = critical failure
        let successStep = -1;

        //Rolling the save from the target actor. WILL NOT WORK OF QUICKROLLS ARE NOT ENABLED

        let roll = result


        if (!roll) {
            return '';
        }

        //Setting the basic roll from its comparison with DC:
        if (roll._total >= dc) {
            successStep = 2;
        } else {
            successStep = 1;
        }

        //applying effects of crits and naturals:
        successStep += critCheck(roll, dc);
        successStep = Math.clamp(successStep, 0, 3);

        let successBy = roll._total - dc;
        switch (successStep) {
            case 0:
                //REIMPLEMENT WITH SETTINGS: t.setTarget(false, { releaseOthers: false, grouSelection: true });
                message =
                    `
                <div class="targetPicker" data-target="${t.data._id}" data-hitType="cm">
                <div style="color:#131516;margin-top:4px;">
                💔 <b>${t.name}:</b>
                </div>
                <div style="border-bottom: 2px solid black;color:#131516;padding-bottom:4px;">
                💔
                <b style="color:#990000">
                Critically failed${(game.settings.get("pf2qr", "ShowExceedsBy") ? ` by ${successBy}!` : `!`)}
                </b>
                </div>
                </div>`;
                if (game.settings.get("pf2qr", "ShowBubbles")) canvas.hud.bubbles.say(t, "💔 Crit Fail", { emote: true });
                break;
            case 1:
                //REIMPLEMENT WITH SETTINGS: t.setTarget(false, { releaseOthers: false, grouSelection: true });
                message =
                    `
                <div class="targetPicker" data-target="${t.data._id}" data-hitType="m">
                <div style="color:#131516;margin-top:4px;">
                ❌ <b>${t.name}:</b>
                </div>
                <div style="color:#131516;border-bottom: 2px solid black;padding-bottom:4px;">
                ❌ Failed${(game.settings.get("pf2qr", "ShowExceedsBy") ? ` by ${successBy}.` : `.`)}
                </div>
                </div>`;
                if (game.settings.get("pf2qr", "ShowBubbles")) canvas.hud.bubbles.say(t, "❌ Fail", { emote: true });
                break;
            case 2:
                message =
                    `
                <div class="targetPicker" data-target="${t.data._id}" data-hitType="h">
                <div style="color:#131516;margin-top:4px;">
                <b style="color:#4C7D4C">✔️</b> <b>${t.name}:</b>
                </div>
                <div style="color:#131516;border-bottom: 2px solid black;padding-bottom:4px;">
                <b style="color:#4C7D4C">✔️</b> Succeeded${(game.settings.get("pf2qr", "ShowExceedsBy") ? ` by ${successBy}.` : `.`)}
                </div>
                </div>`;
                if (game.settings.get("pf2qr", "ShowBubbles")) canvas.hud.bubbles.say(t, `<b style="color:#4C7D4C">✔️</b> Success`, { emote: true });
                break;
            case 3:
                message = //`<div style="color:#131516;  border-bottom: 1px solid black;">${t.name}:   💥 <b style="color:#4C7D4C">Critical Success</b> ${(game.settings.get("pf2qr", "ShowExceedsBy") ? ` by ${successBy}.` : `.`)}</div>`
                    `
                <div class="targetPicker" data-target="${t.data._id}" data-hitType="ch">
                <div style="color:#131516;margin-top:4px;">
                💥 <b>${t.name}:</b>
                </div>
                <div style="border-bottom: 2px solid black;color:#131516;padding-bottom:4px;">
                💥
                <b style="color:#4C7D4C">
                Critically succeeded${(game.settings.get("pf2qr", "ShowExceedsBy") ? ` by ${successBy}!` : `!`)}
                </b>
                </div>
                </div>`;
                if (game.settings.get("pf2qr", "ShowBubbles")) canvas.hud.bubbles.say(t, "💥 Crit Success", { emote: true });
                break;
            default:
                message = `<div style="color:#FF0000">error. Try again, if that doesn't work, your roll command is invalid. Please report it to @Darth_Apples#4725</div>`
                break;
            }
            if (game.user.targets.size > 0) {
                let chatData = {
                    user: game.user._id,
                    content: message
                }
                showResults(chatData);
            }
    
        })
       
        }));
    
        //finishing message:
        let compiledMessage = "<div><h3 style='border-bottom: 3px solid black'>Save Results"/* DC" + dc +*/ + ":</h3>" //ADD A CONFIG FOR THIS IN FUTURE PLEASE!
        compiledMessage += messages.join('');
        compiledMessage += "</div>"
    
        //Determining permissions, and whether to show result or not:
        if (game.user.targets.size > 0) {
            let chatData = {
                user: game.user._id,
                content: compiledMessage
            }
            showResults(chatData);
        }
    
    }
